# Sun/OS Linux 8 Logos packages (81.4-1)
Assets and images for Sun/OS Linux (this also includes logos)

The source code has been moved to Gitlab, located here:
https://gitlab.com/morales-research-corporation/SunOS-Linux/source-rpms/sunoslinux-logos-src
